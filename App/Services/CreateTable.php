<?php
/**
 * Created by PhpStorm.
 * User: DOOMinikP
 * Date: 2018-10-19
 * Time: 17:34
 */

namespace App\Services;


use App\Models\Model;
use PDOException;
use PDO;

class CreateTable extends Model
{
    /**
     * List of users to populate in table
     *
     * @var array
     */
    private $users = [
        ['Jacek','jace@gtestg.pl',758559456],
        ['Agata','aga@gtestg.pl',333444555],
        ['Marcelina','amrc@gtestg.pl',222333444],
        ['Adam','adam@gtewsth.pl',555666777]
    ];

    /**
     * Create new table if not exist
     */
    public function createTable(): void
    {
        try {
            $query = $this->db->prepare("CREATE TABLE IF NOT EXISTS users (
                                           id INT unsigned AUTO_INCREMENT NOT NULL,
                                           name VARCHAR(255),
                                           email VARCHAR(255),
                                           phone CHAR(9),                    
                                           PRIMARY KEY(id))");
            $query->execute();

            $quantity = $this->db->query("SELECT count(*) FROM users");
            $quantity->execute();
            //Check if table users is not empty
            if($quantity->fetchColumn() == 0 )$this->populateTable();

        } catch ( PDOException $e ) {
            echo $e->getMessage();
        }



    }

    /**
     *Populate table
     *
     */
    private function populateTable(): void
    {
        $query = $this->db->prepare('INSERT INTO users VALUES(NULL, :name, :email, :phone)');
        foreach ($this->users as $user) {
            $query->bindValue(':name', $user[0], PDO::PARAM_STR);
            $query->bindValue(':email', $user[1], PDO::PARAM_STR);
            $query->bindValue(':phone', $user[2], PDO::PARAM_STR);
            $query->execute();
        }
    }
}