<?php
/**
 * Created by PhpStorm.
 * User: DOOMinikP
 * Date: 2018-10-19
 * Time: 16:14
 */

namespace App\Views;


class View
{
    /**
     * Return data as a JSON
     *
     * @param $data
     * @return string
     */
    public static function responseJson($data): string
    {
        header("Content-Type: application/json");
        return json_encode($data);
    }
}