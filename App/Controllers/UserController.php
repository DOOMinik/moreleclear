<?php
/**
 * Created by PhpStorm.
 * User: DOOMinikP
 * Date: 2018-10-19
 * Time: 15:47
 */

namespace App\Controllers;
use App\Models\User;
use App\Views\View;

class UserController extends Controller
{
    /**
     * Create proper model to retrieve users from DB
     */
    public function all(): void
    {
        $user = new User();
        echo View::responseJson($user->getAllUsers());
    }
}