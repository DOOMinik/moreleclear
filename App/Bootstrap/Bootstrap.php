<?php
/**
 * Created by PhpStorm.
 * User: DOOMinikP
 * Date: 2018-07-23
 * Time: 08:12
 */

use App\Controllers\UserController;
use App\Services\CreateTable;

//Create Table and populate if necessary
$table = new CreateTable();
$table->createTable();

//Get input data
$uri = trim($_SERVER['REQUEST_URI'],'/');
$method = $_SERVER['REQUEST_METHOD'];

//Match URI
if($uri == 'user' && $method == 'GET'){

    $controller = new UserController();
    $controller->all();
}else{
    header("HTTP/1.0 404 Not Found");
}