<?php
/**
 * Created by PhpStorm.
 * User: DOOMinikP
 * Date: 2018-10-19
 * Time: 15:57
 */

namespace App\Models;

use PDO;
use PDOException;
use Exception;
use App\Services\LogService;

abstract class Model
{
    /**
     * PDO object
     *
     * @var object
     */
    protected $db;

    /**
     * Path to db configuration file
     *
     * @var string
     */
    private $configFile = PATH.'../config/config.ini';

    /**
     * Create PDO object to db connection
     *
     * @throws Exception if file doesn't exist
     */
    public function __construct()
    {
        try {
            if (!is_file($this->configFile)) {
                throw new Exception("No File: ".$this->configFile);
            } else {
                $config = parse_ini_file($this->configFile, true);
            }

            $this->db = new PDO($config['db']['driver'] . 'dbname=' . $config['db']['db_name'] . ';host=' . $config['db']['host'], $config['db']['user'], $config['db']['password']);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException | Exception $e) {
            LogService::addLog(3,$e->getMessage());
            header("HTTP/1.500 Internal Server Error");
            die();
        }
    }
}