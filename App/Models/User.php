<?php
/**
 * Created by PhpStorm.
 * User: DOOMinikP
 * Date: 2018-10-19
 * Time: 16:12
 */

namespace App\Models;

use PDOException;
use PDO;
use App\Services\LogService;

class User extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve all users from DB
     *
     * @return array
     */
    public function getAllUsers(): array
    {
        try {
            $getData = $this->db->prepare("SELECT * FROM users ");
            $getData->execute();

            if ($getData->rowCount() > 0) {
                $data = $getData->fetchAll(PDO::FETCH_ASSOC);
                return $data;
            };

        }catch (PDOException $e){
            LogService::addLog(3,$e->getMessage());
            header("HTTP/1.500 Internal Server Error");
            die();
        }
    }
}